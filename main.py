from confluent_kafka import Consumer
import requests
import json


f = open('config.json')
config = json.load(f)

key_change = config.get("key_mapping")
endpoint = config.get("endpoint")
group_id = config.get("kafka_group")
topic = config.get("topic_name")
bootstrap_servers = config.get("bootstrap_servers")
def send_request_to_api(url, data):

    payload = json.dumps([data
    ])
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    return response.text


def rename_key(data, key_change_dict):
    for key, key_to_change in key_change_dict.items():
        data[key_to_change] = data.pop(key)
    return data

c = Consumer({
    'bootstrap.servers': bootstrap_servers,
    'group.id': group_id,
    'auto.offset.reset': 'earliest'
})

c.subscribe([topic])

while True:
    msg = c.poll(1.0)
    if msg is not None:
        byte_string = msg.value()
        json_data = json.loads(byte_string.decode('utf-8'))
        op = json_data["payload"]["op"]
        print("op")
        if op in ["c", "i", "u"]:
            data = json_data["after"]
            data_to_payload = rename_key(data, key_change)
            response = send_request_to_api(endpoint, data_to_payload)
    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue

    print('Received message: {}'.format(msg))

c.close()